name : "lazybindall"

author: "joykrishnamondal <jm12752@bris.ac.uk>"

keywords:

  "livescript"

  "utility"

  "bindall"

homepage:"https://gitlab.com/sourcevault/lazybindall"

bugs:
  url:"https://gitlab.com/sourcevault/lazybindall/issues"

license:"MIT"

main:"dist/main.js"

repository:"gitlab:sourcevault/lazybindall"

description : "like bindall but lazy ¯\\_(ツ)_/¯"

version : "0.0.2"

devDependencies:

  "livescript":"1.5.0"

  "comma-number":"2.0.0"

  "@sourcevault/bindall":"0.3.1"

scripts:

  test:"node dist/test.js"